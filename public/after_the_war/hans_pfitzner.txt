
Hans Pfitzner

Ohne Seele, ohne Tiefe

Es gibt kein zuverlässigeres Kriterium über die künstlerische "Richtigkeit",
keine sicherere Gewahr fur das Gelingen, als sich leiten zu lassen von dem
Bewußtsein hoherer Art, vulgo dem Einfälleabwarten, dem gegenüber der Weg
des Verstandesbewußtseins ein Umweg ist, der nie zum Ziele führt,
ein Tappen in kunstfremdem Gebiet.

Gewiß, die S p här e , in der irgend Kunst vor sich geht, macht gewaltigen
Unterschied; sie ist es auch, nach der unser Kunstleben sozusagen in Kasten
eingeteilt wird ; und auch mir ist - ich gesteh es - eine Grübelei von
Brahms interessanter, eine trockne Stelle bei Beethoven näherstehend,
als der glücklichste Einfall von Neßler. Jedoch, daß man mit einem niederen
Bereich nichts zu tun haben will, darf einen nicht übersehen lassen, daß es
in diesem auch Offenbarungen geben kann. Und sicher ist subjektive das
Entzücken Beethovens bei seiner herrlichsten Eingebung nicht größer gewesen,
als das eines auf sehr bescheidenem Gebiet komponierenden Musikers mit
gewissem Talent dazu, der mir einmal sagte: "Heut morgen - da hab' ich was
komponiert, das war so schön, daß mir die Tränen in die Augen gekommen sind."
Dies hat nicht nur eine lächerliche Seite.
Es gibt auch geniale Gassenhauer, und die Fähigkeit,
glückliche Einfälle zu haben, ist es, die auch im niederen Genre in diesen
Momenten den damit Begabten immerhin am Wesen der Kunst teilhaftig werden läßt.

Was wir zu verlieren im Begriffe sind, das liegt klar da; es drückt als
Kultur eine Idee aus, die, in Zukunft einem uns entsprechenden Volke
vielleicht noch mehr bedeuten wird, als uns das Griechentum.
Was wir eintauschen, dafür liegen sozusagen zwei "Angebote" vor.
Die international-atonale Strömung ist noch nicht "akzeptiert"‚ sie kann einem
Volke nur aufgezwungen werden. Das atonale Chaos, nebst den ihm
entsprechenden Formen der anderen Künste ist die künstlerische Parallele
zu dem Bolschewismus der dem staatlichen Europa droht. Von dieser Gruppe
will im Grunde niemand etwas wissen; sie wird der Welt aufgezwungen durch
eine Minderzahl, mit Gewalt, - so wie den Weltkrieg Millionen nicht wollten,
wohl aber vier bis fünf Schurken, die ihn durchgesetzt haben. Diese Gruppe
tötet den K ö r p e r , der Welt hier, wie der Kunst dort. Das zweite
Angebot aber ist akzeptiert ist fertig und schon da!
Es ist die Jazz-Foxtrott-Flut, der musikalische Ausdruck des
Am erik a n i s m u s , dieser Gefahr für Europa. Dieser tötet die
S e e l e , und schmeichelt dem Körper, Weshalb seine Gefahr unbemerkt
bleibt und er willkommen ist. Welcher Erscheinung in ihm Weg und Platz
bereitet wird, durch unser Treiben und die allgemeine Weltentwiclung,
emportaucht und an die Stelle unserer unter Mißhandlungen sterbenden Kultur
tritt, davon kann sich ein Bild machen, wer so ein amerikanisches
Jazzband-Conzert hört. Von je habe ich eine, dem Grauen verwandte,
Abneigung gehabt gegen Cirkus, Wintergarten und ähnliche Institute und
Vergnügungen. Aber so lange das eine Welt für sich war, mit seinem
Publikum und seinen K ü n s t l e rn , und die eigentliche hohe Kunst
der Conzerte und Theater scharf getrennt daneben und darüber bestand,
konnte die Existenz der ersteren ja einem nichts anhaben. Man konnte jene
aufsuchen, und diese meiden, und schied sich somit Von einer anderen Welt
los, die seinige aufzusuchen, die immer Vorhanden war. Jetzt sehe ich
die eine Welt ganz verschwinden und die andere auftauchen - ja, sie ist
schon da und tritt ihren Siegeszug durch Europa an, alles zermalmend -
die amerikanischen Tanks der Geisterschlacht gegen europäische Kultur!

Ein Zirkusraum, Vollgefüllt bis auf den letzten Platz, eine nach tausenden
zählende und - zahlende Menge. Im verarmten Deutschland! Ernste Concerte und
Vorstellungen klassischer Opern sind meist leer.

Die Vorträge beginnen; von der ersten Note an wird das Orchester,
oder der Solist, grell beleuchtet, rot, lila, grün, weiß, - je nach
dem Stück oder der Tonart wechselt die Farbe. Alle Darbietungen
tragen dabei den Stempel der Vollkommenheit. Alles wesentliche
auswendig gespielt. Es müssen hundert Proben vorhergegangen sein. Das
dreieinhalbtausendköpfige Publikum jubelt, glotzt, lacht laut bei
besonders grotesken Klängen, scheint innig vertraut mit dem Wesen
und Gehaben der neuen Kunst und mit dem Dirigenten, den zeit-weilig in
mimischen Contakt mit der Masse, den tosenden Beifall beschwichtigend,
mit halber Wendung sein "Zugabbä?" inhclien Riesenräumthineinflragt,
aus dem ihm prompt "Valencia" oder sonst ein ame einer erei s popu aren
Programmnummer entgegentönt, die als "Zugabbä" dann auch gewahrt wird,
nebst vielen anderen. Mit Neidgefühlen erlebt man diese ausgeprobte,
virtuose Vollendung an einer Sache, die, dem künstlerischen Gehalt nach
der Sphäre des Circus, der Equilibristik, des Variete angehört. Die
vollendetste, unfehlbarste Technik hat vielleicht der Akrobat, der
Seiltanzer, Trapezkunstler, weil er seinen Mangel an Vollendung mit
seinen Knochen, seinem Leben be- zahlen müßte; und weil er Körper
ist ohne Seele‘. Die Seele, ohne diekeine hohe Kunst denkbar ist,
spielt in deren Technik hinein, farbt sie, macht SfIGkVEI- gessen,
ja, stört sie. Mit den gleichen Gefühlen aber wie bei den per e SH,
halsbrecherischen, komischen, bengalisch beleuchteten Produktionen im
Circus erlebt man hier den Abend, hört die grotesken, nasal-quietschenden,
rasl- selnden, zum Lachen reizenden, in berechnet schneller Abwechslung
säch : gender Rhythmen, Geräusche und Klänge, staunt über die
verbluffen e hltt tuosität des Saxophonbläsers, der, weiß beleuchtet,
mit souveraner Sicher ei seine rasenden Läufe zum besten gibt, sieht hin,
was der Posaunist da auf seinem Instrument macht, und wie er es macht,
läßt drei Sänger über sich ergehen, die, ebenfalls vom
Schweinwerfer bedient, quäkende Töne in vollendetem Drill ineinanderziehen,
um das Vierteltonsystem anscheinend zu propagieren
und zugleich das Publikum zu amüsieren; alles als Ausführung vollkommen
- als Kunstgattung ins Eminente gesteigertes Caféhaus und Variété,
ohne Seele, ohne Tiefe und Gehalt, fern vom Bereich des Schönen, uns
wesensfremd, Ohren- und Lachkitzel, Sensation, Betäubung, tönende Gemeinheit.
Das Publikum ist geradezu fasziniert, beglückt, von diesem seelenlosen
amerikanischen Maschinismus, der mich unsagbar abstößt. Das Gefühl, das ich
dabei habe, ist schwer zu beschreiben - etwas heimatloses, unsolides, fast
beängstigendes erfaßt mich, wie wenn ich in üble, feindliche
Gesellschaft geraten wäre, deren Sprache ich nicht verstehe:
Hier gehöre ich nicht hin, heraus,
nachhause, zu meinesgleichen! Andere hören es objektiv an, das Interesse
überwiegt, besonders die Jugend, die so vieles assimiliert, teilt wohl kaum
meine Gefühle. Die Freude sei ihr gegönnt - aber sie sieht die Gefahr nicht der
Überschwemmung durch diese Welle, die bald das Festland unserer hohen
Kunst in internationalen Schlamm verwandelt haben wird. Und die Hauptmasse
des Publikums nimmt schon, halb unbewußt, Partei; es fühlt eine Alternative,
gibt dem Untergehenden einen Tritt, die Begeisterung wird aggressiv
gegen Andersfühlende - genau wie in atonalen Conzerten.
U n s re Kunst, in ihren edelsten und höchsten Erscheinungen durfte von je ruhig verspottet und geschmäht werden.
Die Jazzwelt bedeutet die Niedrigkeit, die Aharmonik den Wahnsinn
gegenüber hoher Kunstmusik; demgemäß wird das Publikum beider Richtungen ausfallen. Das will heißen: Die erstere wird eines haben, hat es schon - die
zweite nie.

 

