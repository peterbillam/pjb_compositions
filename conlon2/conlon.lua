#!/usr/bin/env lua
---------------------------------------------------------------------
--     This Lua5 script is Copyright (c) 2020, Peter J Billam      --
--                         pjb.com.au                              --
--  This script is free software; you can redistribute it and/or   --
--         modify it under the same terms as Lua5 itself.          --
---------------------------------------------------------------------
local Version = '1.0  for Lua5'
local VersionDate  = '11apr2020'
local Synopsis = [[
conlon.lua [options] [filenames]
]]
MG = require 'midigenerators'
local iarg=1; while arg[iarg] ~= nil do
	if not string.find(arg[iarg], '^-[a-z]') then break end
	local first_letter = string.sub(arg[iarg],2,2)
	if first_letter == 'v' then
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate)
		os.exit(0)
	elseif first_letter == 'c' then
		whatever()
	else
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate.."\n\n"..Synopsis)
		os.exit(0)
	end
	iarg = iarg+1
end

-- 20200313
--  1) cresc-and-dim
-- 20200314
--  2) also legarg, for articulation
--  3) cha0 left cha1 centre cha2 right; cha3 left Ped cha4 right Ped
--  4) hairpins not just for cresc and dim, but also for pitch
--  5) sequences, not just cycles   


------------------------------ private ------------------------------
local function warn(...)
	local a = {}
	for k,v in pairs{...} do table.insert(a, tostring(v)) end
	io.stderr:write(table.concat(a),'\n') ; io.stderr:flush()
end
local function die(...) warn(...);  os.exit(1) end
function qw(s)  -- t = qw[[ foo  bar  baz ]]
	local t = {} ; for x in s:gmatch("%S+") do t[#t+1] = x end ; return t
end
local function round(x)
	if not x then return nil end
	return math.floor(x+0.5)
end

-- require 'DataDumper'

MG.add_event({'patch_change', 10, 0, 0})
MG.add_event({'control_change', 10, 0, 64, 0})
MG.add_event({'patch_change', 10, 1, 0})
MG.add_event({'control_change', 10, 1, 64, 127})
local s
s = MG.sigmoid_scale(1500,2700,40,100,0,100,100)
s = MG.sigmoid_scale(2000,2400,40,100,0,100,100)
s = MG.sigmoid_scale(4000,2700,40,100,0,100,100)
s = MG.sigmoid_scale(4500,2200,40,100,0,100,100)
s = MG.sigmoid_scale(5000,1700,40,100,0,100,100)
s = MG.trill(6700,2300,101,99,60,0,110,160)
s = MG.trill(7700,4300,33,34,70,0,120,150)
s=MG.sequence(9000,7000,99,{-6,-5,10,-1},{71,151,71,71,83},0,{120,103,210},190)
s = MG.trill(12000,4000,26,28,70,0,120,150)

-- volarg = MG.new_rise_and_fall(10,125)
-- s = MG.trill(16000,3000,76,78,60,0,50,180)  -- high trills pp
-- s = MG.trill(16000,3000,84,86,65,0,50,180)
volarg = MG.new_gen07( {10 , 15, 125, 10, 125, 15, 10} )
s = MG.trill(16000,7000,76,78,60,0,volarg,180)  -- high trills pp
volarg = MG.new_gen07( {1 , 15, 125, 16, 125, 15, 1} )
s = MG.trill(16000,7000,84,86,65,0,volarg,180)
s = MG.pedal(16100,2900,0)

s = MG.trill(19000,4000,26,28,70,0,120,150) -- ascent
s = MG.trill(23000,2900,33,34,70,0,120,160) -- ascent leaves a B_b at the end
s = MG.sequence(19000,7000,27,{6,5,-10,1},{50,127,61,83,51},0,{120,103,210},190)
s = MG.chord(26000,4000,{48,54,58,64,69,74,79},0,80)
s = MG.chord(30000,2800,{37,40,43,47,51,54,58,62,66,68,70,73,77,80},0,100)
-- pp
volarg = MG.new_gen07( {1 , 15, 95, 5, 95, 15, 1} )
s = MG.sequence(32000,20000,37,{3,3,4,4,3,4,4,4,2,2,3,4,3,-3,-4,-3,-2,-2,-4,-4,-4,-3,-4,-4,-3,-3},80,1,volarg,110)
s = MG.sequence(32010,20000,37,{3,3,4,4,3,4,4,4,2,2,3,4,3,-3,-4,-3,-2,-2,-4,-4,-4,-3,-4,-4,-3,-3},80,1,volarg,110)

t = 33000 ; pitcharg = MG.new_igrand(48, 5)
while t < 50000 do
	local dt = MG.rayleigh_irand(120)
	MG.add_event( {'note',t,dt,0,pitcharg(),110} )
	t = t + dt
end

MG.write_score()

--[=[

=pod

=head1 NAME

conlon.lua - does whatever

=head1 AUTHOR

Peter J Billam, http://pjb.com.au/comp/contact.html

=head1 SEE ALSO

 http://pjb.com.au/


=cut

]=]

